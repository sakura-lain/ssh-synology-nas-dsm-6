# SSH on a Synology NAS running DSM 6

If you're not familiar with SSH, please take a look at [my SSH reminder](https://gitlab.com/sakura-lain/ssh-reminder).

## How to enable a SSH key on a Synology NAS running DSM 6.

- After having created a key and enabled ssh-agent, you can copy it to your Synology NAS:

```
ssh-copy-id -i ~/.ssh/id_rsa.pub user@<IP_Syno>
```

Or, to add all the keys stored on your computer:

```
ssh-copy-id user@<IP_Syno>
```

You can check that keys were actually added by checking your `~/.ssh/authorized_keys` file on your NAS.

- Then connect to the NAS using your SSH password to set it up (and don't forget to use `sudo` to run the following commands when necessary).

- In the `/etc/sshd_config file`, uncomment the following lines to enable key authentication:

```
PubkeyAuthentication yes
AuthorizedKeysFile .ssh/authorized_keys
```

- Change a few permissions:

```
chmod 755 /volume1/homes/user
chmod 700 /volume1/homes/user/.ssh
chmod 644 /volume1/homes/user/.ssh/authorized_keys
```

That's done! You can now disconnect and reconnect to test, this time your NAS should't ask for your password.
<br/>To add a second key, just repeat the first step.

**Note:** On DSM 5 it's even simpler since you just have to add your public key to the NAS and let's go!

**Sources:** 
- [http://gurau-audibert.hd.free.fr/josdblog/2014/04/authentification-par-cle-ssh-linux-synology-vera/](http://gurau-audibert.hd.free.fr/josdblog/2014/04/authentification-par-cle-ssh-linux-synology-vera/)
- [https://techarea.fr/tuto-ssh-cle-nas-synology/](https://techarea.fr/tuto-ssh-cle-nas-synology/)


